#!/usr/bin/env python

from random import randint
import smtplib
import sys

smtp = smtplib.SMTP('smtp.gmail.com', 587)
smtp.ehlo()
smtp.starttls()
smtp.login('secretsantafun13@gmail.com', 'secretsantafun123')

santas = [
    'georgiana.tepelus@yahoo.com',
    'afk.paul@ymail.com',
    'graur_iulia@yahoo.com',
    'dedulescu_ionut@yahoo.com',
    'ileana.isar@gmail.com',
    'radu.graur@gmail.com',
    'ramona_dinca1@yahoo.com',
    'hash8484@yahoo.com',
    'monicatepelus@yahoo.com',
    'maria.cristian28@gmail.com',
    'lucia.anghelina@yahoo.com',
    'toaderg13@yahoo.com',
    'dre_andrei@yahoo.co.uk'
]

# Avoid boring picks
pairs = [
    'afk.paul@ymail.com',
    'georgiana.tepelus@yahoo.com',
    'dedulescu_ionut@yahoo.com',
    'graur_iulia@yahoo.com',
    'radu.graur@gmail.com',
    'ileana.isar@gmail.com',
    'hash8484@yahoo.com',
    'ramona_dinca1@yahoo.com',
    'maria.cristian28@gmail.com',
    'monicatepelus@yahoo.com',
    'toaderg13@yahoo.com',
    'lucia.anghelina@yahoo.com',
    'single and ready to mingle'
]

# Kids will be Santas some day
kids = santas[:]

picks = {}

for santaIndex, santa in enumerate(santas):
    # Kinda "do while", because... python
    attempts = 0
    while True:
        attempts += 1

        # Maybe not a coincidence
        if(attempts > 100):
            sys.exit('Don\'t blame Santa, he is old')

        # Pick a random kid from the bucket
        kidIndex = randint(0, len(kids) - 1)
        kid = kids[kidIndex]

        # Avoid boring combinations (self or pair)
        if(santa != kid and kid != pairs[santaIndex]):
            break;

    # Save pick, and remove kid
    picks[santa] = kid
    del kids[kidIndex]

def sendLetterToSanta(to, subject, body):
    headers = [
        'From: secret@santa.com',
        'Subject: ' + subject,
        'To: ' + to,
        'MIME-Version: 1.0',
        'Content-Type: text/html'
    ]
    headers = "\r\n".join(headers)

    try:
        # smtp.sendmail(to, to, headers + "\r\n\r\n" + body)
        print '%s -> %s' % (to, body)
    except SMTPException:
        sys.exit('Cannot send mail to Santa')

for pick in picks:
    sendLetterToSanta(pick, 'Secret Santa', picks[pick])

